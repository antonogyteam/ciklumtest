### Ciklum test project

#### Set up

```

composer install

```

#### Usage

```
<sitename>?content=<content>&width=<width>&height=<height>

```

where:
- content - some text to be encoded,
- width - the width of the resulted image,
- height - the height of the resulted image.
