<?php

namespace CiklumTest\Renderer;

/**
 * Interface RendererInterface
 * @package CiklumTest\Renderer
 */
interface RendererInterface
{
    /**
     * Sets the content which should be encrypted.
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Sets the dimensions of the resulted image.
     * @param int $width
     * @param int $height
     * @return $this
     */
    public function setDimensions($width, $height);

    /**
     * Performs the QR code rendering.
     * @return mixed
     */
    public function render();
}
