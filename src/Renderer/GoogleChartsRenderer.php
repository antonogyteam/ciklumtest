<?php

namespace CiklumTest\Renderer;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\RequestOptions;

/**
 * Class GoogleChartsRenderer
 * @package CiklumTest\Renderer
 */
class GoogleChartsRenderer implements RendererInterface
{
    const GOOGLE_CHARTS_API_URL = 'https://chart.googleapis.com/chart';

    /** @var string */
    protected $content;

    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /**
     * @inheritdoc
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setDimensions($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
        return $this;
    }

    /**
     * Returns preferred HTTP method depends on content length.
     *
     * Note that URLs have a 2K maximum length, so if you want to encode more than 2K bytes
     * (minus the other URL characters), you will have to send your data using POST.
     * @see https://developers.google.com/chart/infographics/docs/qr_codes#syntax
     *
     * @return string
     */
    protected function getHttpMethod()
    {
        return (strlen($this->content) >= 2048) ? 'POST' : 'GET';
    }

    /**
     * Returns request params.
     * @see https://developers.google.com/chart/infographics/docs/qr_codes#syntax
     *
     * @return array
     */
    protected function getParams()
    {
        $params = [
            'cht' => 'qr',
            'chs' => "{$this->width}x{$this->height}",
            'chl' => $this->content,
            'choe' => 'UTF-8',

        ];

        return $this->getHttpMethod() == 'POST' ?
            [RequestOptions::FORM_PARAMS => $params] :
            [RequestOptions::QUERY => $params];
    }

    /**
     * @inheritdoc
     */
    public function render()
    {
        $client = new HttpClient();
        $response = $client->request($this->getHttpMethod(), static::GOOGLE_CHARTS_API_URL, $this->getParams());

        if ($response->getStatusCode() != 200) {
            throw new RendererException("Googleapi charts service is not available");
        }

        return $response->getBody();
    }
}
