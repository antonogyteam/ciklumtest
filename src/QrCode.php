<?php

namespace CiklumTest;

use CiklumTest\Renderer\RendererInterface;
use CiklumTest\Renderer\RendererException;

/**
 * Class QrCode
 * @package CiklumTest
 */
class QrCode
{
    /** @var string */
    protected $content;

    /** @var int */
    protected $width;

    /** @var int */
    protected $height;

    /** @var RendererInterface */
    protected $renderer;

    /**
     * QrCode constructor.
     * @param string $content
     * @param int $width
     * @param int $height
     */
    public function __construct($content, $width, $height)
    {
        $this->content = $content;
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Sets the renderer class.
     * @param RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * Generates the QR code and returns it as the result.
     * @throws RendererException|\Exception
     * @return mixed
     */
    public function generate()
    {
        return $this->renderer
            ->setContent($this->content)
            ->setDimensions($this->width, $this->height)
            ->render();
    }
}
