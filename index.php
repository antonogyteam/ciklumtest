<?php

require_once __DIR__ . '/vendor/autoload.php';

$content = isset($_GET['content']) ? $_GET['content'] : null;
$width = isset($_GET['width']) ? $_GET['width'] : null;
$height = isset($_GET['height']) ? $_GET['height'] : null;

$validator = new Particle\Validator\Validator();

$validator->required('content');
$validator->required('width')->digits();
$validator->required('height')->digits();

$result = $validator->validate([
    'content' => $content,
    'width' => $width,
    'height' => $height
]);

if (!$result->isValid()) {
    $errorMessage = json_encode($result->getMessages());
    header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
    header('Content-Type: application/json; charset=utf-8');
    header('Content-Length: ' . strlen($errorMessage));
    echo $errorMessage;
    exit(1);
}

$qrCode = new CiklumTest\QrCode($content, $width, $height);
$qrCode->setRenderer(new CiklumTest\Renderer\GoogleChartsRenderer());

try {
    $qrCodeData = $qrCode->generate();
} catch (Exception $e) {
    // $e->getMessage() may be written to the log
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    exit(1);
}

header('Content-Type: image/png; charset=utf-8');
header('Content-Length: ' . strlen($qrCodeData));
echo $qrCodeData;
exit(0);
